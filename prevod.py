import sys

def prevodXX(rc):
  hodnoty = { 'I': 1,'V':5,'X':10,'L':50,'C':100,'D':500,'M':1000}  
  celkem = 0
  predchozi = 0
  for c in rc[-1::-1]:
    v = hodnoty[c]
    if v >= predchozi:
      celkem = celkem + v
    else:
      celkem = celkem - v
    predchozi = v
  return celkem

def main():
  #print(prevodXX(sys.argv[1]))
  print(prevodXX("CI"))

if __name__ == '__main__':
    main()
    
